ARG INTERNAL_REG
ARG EXTERNAL_REG
ARG IMG_VERSION
FROM ${INTERNAL_REG}/debian:bullseye as base



FROM ${EXTERNAL_REG}/node:$IMG_VERSION-bullseye-slim

SHELL ["/bin/bash", "-c"]

ARG http_proxy
ARG https_proxy

COPY --from=base \
    /etc/ssl/certs/ca-certificates.crt \
    /etc/ssl/certs/ca-certificates.crt
ENV SSL_CERT_FILE=/etc/ssl/certs/ca-certificates.crt

# Install Git
RUN apt-get update \
    && DEBIAN_FRONTEND=noninteractive apt-get install -y --no-install-recommends \
        git \
        locales \
    && DEBIAN_FRONTEND=noninteractive apt-get upgrade -y \
    && rm -rf /var/lib/apt/lists/*

# Set locale
RUN sed -i '/en_US.UTF-8/s/^# //g' /etc/locale.gen && \
    locale-gen
ENV LANG en_US.UTF-8
ENV LANGUAGE en_US:en
ENV LC_ALL en_US.UTF-8

# Add Proxy / SSL for Git, NPM, Yarn
ARG LOCAL_GITLAB_URL
RUN set -euo pipefail && \
    git config --global credential.helper store && \
    git config --global url."https://github.com/".insteadOf git@github.com: && \
    git config --global url."https://".insteadOf git:// && \
    if [[ -z "$http_proxy" ]] ; then \
        echo "No http_proxy set, skipping (Git, NPM, Yarn) config." ; \
    else \
        git config --global http.proxy "${http_proxy}" && \
        git config --global https.proxy "${https_proxy}" && \
        git config --global http.sslCAInfo /etc/ssl/certs/ca-certificates.crt && \
        if [[ -z "$LOCAL_GITLAB_URL" ]] ; then \
            echo "ARG LOCAL_GITLAB_URL not set, skipping Git no_proxy config." ; \
        else \
            git config --global http."${LOCAL_GITLAB_URL}".proxy "" && \
            git config --global https."${LOCAL_GITLAB_URL}".proxy "" ; \
        fi && \
        npm config set proxy "${http_proxy}" && \
        npm config set https-proxy "${https_proxy}" && \
        npm config set no-proxy "${no_proxy}" && \
        npm config set cafile /etc/ssl/certs/ca-certificates.crt && \
        yarn config set proxy "${http_proxy}" && \
        yarn config set https-proxy "${https_proxy}" && \
        yarn config set no-proxy "${no_proxy}" && \
        yarn config set cafile /etc/ssl/certs/ca-certificates.crt ; \
    fi

ARG IMG_VERSION
ARG MAINTAINER
LABEL "NODE_IMG_TAG"="${IMG_VERSION}" \
      "MAINTAINER"="${MAINTAINER}" \
      "SSL_CERT_FILE"="${SSL_CERT_FILE}"
