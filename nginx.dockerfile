ARG EXTERNAL_REG
ARG INTERNAL_REG
ARG IMG_VERSION
FROM ${INTERNAL_REG}/debian:bullseye as certs



FROM ${EXTERNAL_REG}/nginx:${IMG_VERSION} as run
ENV SSL_CERT_FILE=/etc/ssl/certs/ca-certificates.crt
ARG IMG_VERSION
ARG MAINTAINER
LABEL "NGINX_IMG_TAG"="${IMG_VERSION}" \
      "MAINTAINER"="${MAINTAINER}" \
      "SSL_CERT_FILE"="${SSL_CERT_FILE}"
# CA-Certs
COPY --from=certs \
    /etc/ssl/certs/ca-certificates.crt \
    /etc/ssl/certs/ca-certificates.crt