ARG INTERNAL_REG

FROM ${INTERNAL_REG}/debian:bullseye as build

ARG http_proxy
ARG https_proxy

ENV LANG=C.UTF-8 LC_ALL=C.UTF-8

RUN set -ex \
    && BUILD_DEPS=" \
    bzip2 \
    git \
    libglib2.0-0 \
    libsm6 \
    libxext6 \
    libxrender1 \
    mercurial \
    subversion \
    wget \
    " \
    && apt-get update && apt-get install -y --no-install-recommends $BUILD_DEPS \
    && rm -rf /var/lib/apt/lists/*

ARG CONDA_VERSION=py38_4.9.2
ARG CONDA_MD5=122c8c9beb51e124ab32a0fa6426c656

RUN wget --quiet https://repo.anaconda.com/miniconda/Miniconda3-${CONDA_VERSION}-Linux-x86_64.sh -O miniconda.sh && \
    echo "${CONDA_MD5}  miniconda.sh" > miniconda.md5 && \
    if ! md5sum --status -c miniconda.md5; then exit 1; fi
RUN mkdir -p /opt && \
    sh miniconda.sh -b -p /opt/conda && \
    rm miniconda.sh miniconda.md5
RUN find /opt/conda/ -follow -type f -name '*.a' -delete && \
    find /opt/conda/ -follow -type f -name '*.js.map' -delete && \
    /opt/conda/bin/conda clean -afy



FROM ${INTERNAL_REG}/debian:bullseye as runtime

ARG http_proxy
ARG https_proxy

ENV LANG=C.UTF-8 LC_ALL=C.UTF-8

SHELL ["/bin/bash", "--login", "-c"]

COPY --from=build /opt/conda /opt/conda
ENV PATH /opt/conda/bin:$PATH

# Symlink and activate conda in .bashrc
# Must reference absolute conda install, as ENV above not used by bash login shell in Dockerfile
RUN set -euo pipefail && \
    ln -s /opt/conda/etc/profile.d/conda.sh /etc/profile.d/conda.sh && \
    echo ". /opt/conda/etc/profile.d/conda.sh" >> ~/.bashrc && \
    echo "conda activate base" >> ~/.bashrc && \
    /opt/conda/bin/conda config --set ssl_verify True && \
    /opt/conda/bin/conda config --set ssl_verify /etc/ssl/certs/ca-certificates.crt && \
    if [[ -z "$http_proxy" ]] ; then \
        echo "No http_proxy set, skipping conda proxy config." ; \
    else \
        /opt/conda/bin/conda config --set proxy_servers.http ${http_proxy} && \
        /opt/conda/bin/conda config --set proxy_servers.https ${https_proxy} ; \
    fi

WORKDIR /opt/conda

ENTRYPOINT ["/bin/bash", "-c"]

ARG IMG_VERSION
ARG MAINTAINER
LABEL "CONDA_VERSION"="${IMG_VERSION}" \
      "MAINTAINER"="${MAINTAINER}"
