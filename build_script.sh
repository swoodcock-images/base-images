#!/bin/bash
set -euo pipefail

export DOCKER_BUILDKIT=1
export BUILDKIT_STEP_LOG_MAX_SIZE=1073741824
export BUILDKIT_STEP_LOG_MAX_SPEED=10240000

source .env

declare -a versions=("bullseye")
for ver in "${versions[@]}"
do
   echo Building Debian: "$ver"
   docker build . -f "debian.dockerfile" \
      -t "${INTERNAL_REG}/debian:$ver" \
      --build-arg http_proxy=$proxy_url \
      --build-arg https_proxy=$proxy_url \
      --build-arg no_proxy=$no_proxy \
      --build-arg MAINTAINER=$MAINTAINER \
      --build-arg EXTERNAL_REG=$EXTERNAL_REG \
      --build-arg IMG_VERSION=$ver
   if [[ "${PUSH_IMAGES}" = true ]] ; then
      docker push "${INTERNAL_REG}/debian:$ver" ;
   else
      echo "Skipped pushing ${INTERNAL_REG}/debian:$ver" ;
   fi
done

declare -a versions=("3.10")
for ver in "${versions[@]}"
do
   echo Building Python: "$ver"
   docker build . -f "python.dockerfile" \
      -t "${INTERNAL_REG}/python:$ver" \
      --build-arg http_proxy=$proxy_url \
      --build-arg https_proxy=$proxy_url \
      --build-arg no_proxy=$no_proxy \
      --build-arg MAINTAINER=$MAINTAINER \
      --build-arg INTERNAL_REG=$INTERNAL_REG \
      --build-arg EXTERNAL_REG=$EXTERNAL_REG \
      --build-arg IMG_VERSION=$ver
   if [[ "${PUSH_IMAGES}" = true ]] ; then
      docker push "${INTERNAL_REG}/python:$ver" ;
   else
      echo "Skipped pushing ${INTERNAL_REG}/python:$ver" ;
   fi
done

declare -a versions=("3.15" "3.16")
for ver in "${versions[@]}"
do
   echo Building Alpine: "$ver"
   docker build . -f "alpine.dockerfile" \
      -t "${INTERNAL_REG}/alpine:$ver" \
      --build-arg http_proxy=$proxy_url \
      --build-arg https_proxy=$proxy_url \
      --build-arg no_proxy=$no_proxy \
      --build-arg MAINTAINER=$MAINTAINER \
      --build-arg INTERNAL_REG=$INTERNAL_REG \
      --build-arg EXTERNAL_REG=$EXTERNAL_REG \
      --build-arg IMG_VERSION=$ver
   if [[ "${PUSH_IMAGES}" = true ]] ; then
      docker push "${INTERNAL_REG}/alpine:$ver" ;
   else
      echo "Skipped pushing ${INTERNAL_REG}/alpine:$ver" ;
   fi
done

declare -a versions=("1.19")
for ver in "${versions[@]}"
do
   echo Building Golang: "$ver"
   docker build . -f "golang.dockerfile" \
      -t "${INTERNAL_REG}/golang:$ver" \
      --build-arg http_proxy=$proxy_url \
      --build-arg https_proxy=$proxy_url \
      --build-arg no_proxy=$no_proxy \
      --build-arg MAINTAINER=$MAINTAINER \
      --build-arg INTERNAL_REG=$INTERNAL_REG \
      --build-arg EXTERNAL_REG=$EXTERNAL_REG \
      --build-arg IMG_VERSION=$ver
   if [[ "${PUSH_IMAGES}" = true ]] ; then
      docker push "${INTERNAL_REG}/golang:$ver" ;
   else
      echo "Skipped pushing ${INTERNAL_REG}/golang:$ver" ;
   fi
done

declare -a versions=("19" "16")
for ver in "${versions[@]}"
do
   echo Building Node: "$ver"
   docker build . -f "node.dockerfile" \
      -t "${INTERNAL_REG}/node:$ver" \
      --build-arg http_proxy=$proxy_url \
      --build-arg https_proxy=$proxy_url \
      --build-arg no_proxy=$no_proxy \
      --build-arg MAINTAINER=$MAINTAINER \
      --build-arg INTERNAL_REG=$INTERNAL_REG \
      --build-arg EXTERNAL_REG=$EXTERNAL_REG \
      --build-arg LOCAL_GITLAB_URL=$LOCAL_GITLAB_URL \
      --build-arg IMG_VERSION=$ver
   if [[ "${PUSH_IMAGES}" = true ]] ; then
      docker push "${INTERNAL_REG}/node:$ver" ;
   else
      echo "Skipped pushing ${INTERNAL_REG}/node:$ver" ;
   fi
done

declare -a versions=("1.23.2")
for ver in "${versions[@]}"
do
   echo Building Nginx: "$ver"
   docker build . -f "nginx.dockerfile" \
      -t "${INTERNAL_REG}/nginx:$ver" \
      --build-arg MAINTAINER=$MAINTAINER \
      --build-arg INTERNAL_REG=$INTERNAL_REG \
      --build-arg EXTERNAL_REG=$EXTERNAL_REG \
      --build-arg IMG_VERSION=$ver
   if [[ "${PUSH_IMAGES}" = true ]] ; then
      docker push "${INTERNAL_REG}/nginx:$ver" ;
   else
      echo "Skipped pushing ${INTERNAL_REG}/nginx:$ver" ;
   fi
done

declare -a versions=("4.9")
for ver in "${versions[@]}"
do
   echo Building Conda: "$ver"
   docker build . -f "conda.dockerfile" \
      -t "${INTERNAL_REG}/conda:$ver" \
      --build-arg http_proxy=$proxy_url \
      --build-arg https_proxy=$proxy_url \
      --build-arg no_proxy=$no_proxy \
      --build-arg MAINTAINER=$MAINTAINER \
      --build-arg INTERNAL_REG=$INTERNAL_REG
   if [[ "${PUSH_IMAGES}" = true ]] ; then
      docker push "${INTERNAL_REG}/conda:$ver" ;
   else
      echo "Skipped pushing ${INTERNAL_REG}/conda:$ver" ;
   fi
done
