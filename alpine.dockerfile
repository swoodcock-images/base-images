ARG INTERNAL_REG
ARG EXTERNAL_REG
ARG IMG_VERSION
FROM ${INTERNAL_REG}/debian:bullseye as certs



FROM ${EXTERNAL_REG}/alpine:$IMG_VERSION

ARG http_proxy
ARG https_proxy

# CA-Certs
COPY --from=certs \
    /etc/ssl/certs/ca-certificates.crt \
    /etc/ssl/certs/ca-certificates.crt
ENV SSL_CERT_FILE=/etc/ssl/certs/ca-certificates.crt
# Upgrade packages
RUN apk -U upgrade

ARG IMG_VERSION
ARG MAINTAINER
LABEL "ALPINE_IMG_TAG"="${IMG_VERSION}" \
      "MAINTAINER"="${MAINTAINER}" \
      "SSL_CERT_FILE"="${SSL_CERT_FILE}"
