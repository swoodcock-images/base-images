ARG INTERNAL_REG
ARG EXTERNAL_REG
ARG IMG_VERSION
FROM ${INTERNAL_REG}/debian:bullseye as certs



FROM ${EXTERNAL_REG}/golang:$IMG_VERSION-alpine

ARG http_proxy
ARG https_proxy

# CA-Certs
COPY --from=certs \
    /etc/ssl/certs/ca-certificates.crt \
    /etc/ssl/certs/ca-certificates.crt
ENV SSL_CERT_FILE=/etc/ssl/certs/ca-certificates.crt
RUN apk -U upgrade

ARG IMG_VERSION
ARG MAINTAINER
LABEL "GOLANG_IMG_TAG"="${IMG_VERSION}" \
      "MAINTAINER"="${MAINTAINER}" \
      "SSL_CERT_FILE"="${SSL_CERT_FILE}"
