ARG INTERNAL_REG
ARG EXTERNAL_REG
ARG IMG_VERSION
FROM ${INTERNAL_REG}/debian:bullseye as certs



FROM ${EXTERNAL_REG}/python:${IMG_VERSION}-slim-bullseye as build

ARG http_proxy
ARG https_proxy

# CGG Proxy & Cert
COPY --from=certs \
    /etc/ssl/certs/ca-certificates.crt \
    /etc/ssl/certs/ca-certificates.crt

WORKDIR /opt/python

RUN set -ex \
    && BUILD_DEPS=" \
    build-essential \
    gcc \
    libpcre3-dev \
    libpq-dev \
    libspatialindex-dev \
    libproj-dev \
    libgeos-dev \
    # libgdal-dev \
    git \
    " \
    && apt-get update && apt-get install \
        -y --no-install-recommends $BUILD_DEPS \
    && echo "deb http://deb.debian.org/debian bookworm main" >> /etc/apt/sources.list \
    && apt-get update && apt-get install \
        -y --no-install-recommends -t bookworm \
            "libgdal-dev" \
    && rm -rf /var/lib/apt/lists/*

ARG IMG_VERSION
COPY pyproject.toml pdm.lock ./
RUN pip install --no-cache-dir --upgrade pip \
    && pip install --no-cache-dir pdm \
    && pdm config python.use_venv false
RUN pdm install -G geo --prod --no-editable --no-self
RUN pip install -t $(pdm info --packages)/lib gdal==3.6.2



FROM python:${IMG_VERSION}-slim-bullseye as runtime

ARG http_proxy
ARG https_proxy
ARG IMG_VERSION
ARG MAINTAINER
# CGG Proxy + CA-Certs
# PYTHONFAULTHANDLER faulthandler for base level C code
# PYTHONDONTWRITEBYTECODE Don't write .pyc on execution, pre-populate instead
# PYTHONUNBUFFERED Force the stdout and stderr streams to be unbuffered
ENV PYTHONDONTWRITEBYTECODE=1 \
    PYTHONUNBUFFERED=1 \
    PYTHONFAULTHANDLER=1 \
    SSL_CERT_FILE=/etc/ssl/certs/ca-certificates.crt \
    REQUESTS_CA_BUNDLE=/etc/ssl/certs/ca-certificates.crt \
    CURL_CA_BUNDLE=/etc/ssl/certs/ca-certificates.crt \
    PATH="/opt/python/bin:$PATH" \
    PYTHONPATH="/opt/python/pkgs:/opt/python/pkgs/pdm/pep582"
LABEL "PYTHON_IMG_TAG"="${IMG_VERSION}" \
      "MAINTAINER"="${MAINTAINER}" \
      "PYTHONDONTWRITEBYTECODE"="True" \
      "PYTHONUNBUFFERED"="True" \
      "PYTHONFAULTHANDLER"="True" \
      "PYTHONPATH"="$PYTHONPATH" \
      "SSL_CERT_FILE"="${SSL_CERT_FILE}" \
      "REQUESTS_CA_BUNDLE"="${REQUESTS_CA_BUNDLE}" \
      "CURL_CA_BUNDLE"="${CURL_CA_BUNDLE}"

COPY --from=certs \
    /etc/ssl/certs/ca-certificates.crt \
    /etc/ssl/certs/ca-certificates.crt

# Install packages needed to run your application (not build deps):
#   mime-support -- for mime types when serving static files
#   postgresql-client -- for running database commands
#   libgdal32 -- for GDAL 3.6.x  |  libgdal28 -- for GDAL 3.2.x
#   libspatialindex-c6 -- for RTree
RUN set -ex \
    && RUN_DEPS=" \
    locales \
    libpcre3 \
    mime-support \
    postgresql-client \
    libglib2.0-0 \
    libspatialindex-c6 \
    libproj19 \
    libgeos-c1v5 \
    # libgdal28 \
    " \
    && apt-get update \
    && DEBIAN_FRONTEND=noninteractive apt-get install \
       -y --no-install-recommends $RUN_DEPS \
    && echo "deb http://deb.debian.org/debian bookworm main" >> /etc/apt/sources.list \
    && apt-get update && apt-get install \
        -y --no-install-recommends -t bookworm \
            "libgdal32" \
    && rm -rf /var/lib/apt/lists/*

# Set locale
RUN sed -i '/en_US.UTF-8/s/^# //g' /etc/locale.gen && \
    locale-gen
ENV LANG en_US.UTF-8
ENV LANGUAGE en_US:en
ENV LC_ALL en_US.UTF-8

WORKDIR /opt/python

COPY --from=build \
    "/opt/python/__pypackages__/${IMG_VERSION}/lib" \
    /opt/python/pkgs
COPY --from=build \
    "/opt/python/__pypackages__/${IMG_VERSION}/bin" \
    /opt/python/bin

# Upgrade PIP
# Set PDM default to PEP582
# Compile deps to .pyc for speed gains
RUN python -m pip install --no-cache --upgrade pip \
    && pdm config --global python.use_venv false \
    && python -c "import compileall; compileall.compile_path(maxlevels=10, quiet=1)"
