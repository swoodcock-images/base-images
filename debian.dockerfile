ARG EXTERNAL_REG
ARG IMG_VERSION

FROM ${EXTERNAL_REG}/debian:$IMG_VERSION as build

ARG http_proxy
ARG https_proxy

# CA-Certs
COPY certs/dummy.crt certs/* /usr/local/share/ca-certificates/
RUN apt-get update && apt-get install -y --no-install-recommends \
        ca-certificates \
    && rm -rf /var/lib/apt/lists/* \
    && chmod -R 644 /usr/local/share/ca-certificates \
    && update-ca-certificates



FROM ${EXTERNAL_REG}/debian:$IMG_VERSION as runtime

ARG http_proxy
ARG https_proxy

# CA-Certs
COPY --from=build \
    /etc/ssl/certs/ca-certificates.crt \
    /etc/ssl/certs/ca-certificates.crt
ENV SSL_CERT_FILE=/etc/ssl/certs/ca-certificates.crt

# Upgrade packages
RUN apt-get update\
    && DEBIAN_FRONTEND=noninteractive apt-get upgrade -y \
    && DEBIAN_FRONTEND=noninteractive apt-get install -y \
        locales \
    && rm -rf /var/lib/apt/lists/*

# Set locale
RUN sed -i '/en_US.UTF-8/s/^# //g' /etc/locale.gen && \
    locale-gen
ENV LANG en_US.UTF-8
ENV LANGUAGE en_US:en
ENV LC_ALL en_US.UTF-8

ARG IMG_VERSION
ARG MAINTAINER
LABEL "DEBIAN_IMG_TAG"="${IMG_VERSION}" \
      "MAINTAINER"="${MAINTAINER}" \
      "SSL_CERT_FILE"="${SSL_CERT_FILE}"
